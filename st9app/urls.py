from django.urls import path, include
from .views import index, login_func, logout_func

app_name="st9app"

urlpatterns = [
    path('', index, name="st9"),
    path('login/', login_func, name="login"),
    path('logout/', logout_func, name="logout")
]