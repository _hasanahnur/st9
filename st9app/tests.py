from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse, resolve

from .views import index, login_func, logout_func


class TestStory7(TestCase):
    
    def test_views_index(self):
        handler = resolve('/')
        self.assertEqual(handler.func, index)

    def test_template_index(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')
        
    def test_views_logout(self):
        handler = resolve('/logout/')
        self.assertEqual(handler.func, logout_func)

    def test_views_login(self):
        handler = resolve('/login/')
        self.assertEqual(handler.func, login_func)
