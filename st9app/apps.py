from django.apps import AppConfig


class St9AppConfig(AppConfig):
    name = 'st9app'
